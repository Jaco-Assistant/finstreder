# Finstreder

The Finite-State-Transducer-Decoder library helps decoding CTC-outputs with FSTs using [OpenFST](http://www.openfst.org/twiki/bin/view/FST/WebHome).
An overview of the approach can be found in the paper [Finstreder: Simple and fast Spoken Language Understanding with Finite State Transducers using modern Speech-to-Text models](https://arxiv.org/pdf/2206.14589.pdf).

[![pipeline status](https://gitlab.com/Jaco-Assistant/finstreder/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/finstreder/-/commits/master)
[![coverage report](https://gitlab.com/Jaco-Assistant/finstreder/badges/master/coverage.svg)](https://gitlab.com/Jaco-Assistant/finstreder/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/finstreder/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/finstreder/-/commits/master)

<br>

## Installation

Usage requires a working installation of [OpenFST](http://www.openfst.org/twiki/bin/view/FST/WebHome). \
Check out the installation steps in the [Containerfile](tests/Containerfile) used for testing.

Install finstreder client:

```bash
git clone https://gitlab.com/Jaco-Assistant/finstreder.git
pip3 install --user -e finstreder/
```

<br>

## Usage

The following examples shows building a Finite-State-Transducer-Decoder step-by-step with two very simple usecases. \
In the first a simple decoder for sentences is built using a text-file, the second shows an example for usage as speech-to-intent decoder.

(For fast testing of this tool, the testing container can be used. The steps here are using the test examples.
You can either build the container yourself, as described at the end of this file, or pull it from the project's registry.)

<br>

### 1) Simple Grammar

Generate files for all used symbols: \
This requires a text file with one sentence per line and a list of alphabet characters in json format.

```bash
# Go to the working directory
cd /finstreder/tests/outputs/txt-fixed/

python3 /finstreder/finstreder/create_symbols.py \
  --sentences_path /finstreder/tests/data/sentences_mini.txt \
  --alphabet_path /finstreder/tests/data/alphabet_mini_chars.json \
  --workdir /finstreder/tests/outputs/txt-fixed/
```

<br>

Generate the T(oken), L(exicon) and G(rammar) FSTs:

```bash
# Build text representations for Token and Lexcion FSTs
# All words in the lexicon end with a <space> which is used as disambiguation symbol here
python3 /finstreder/finstreder/create_TL_fsts.py \
  --workdir /finstreder/tests/outputs/txt-fixed/

# There are multiple ways to build the Grammar-FST:
# 1. A plain text file with one sentence per line, like in this example
# 2. Using a json file with intents in Jaco's syntax style, see later
# 3. Using an n-gram model instead, either with sentences or with intents, see later
# Methods 1 and 2 are only recommended if only a few sentences have to be recognized, they also restrict the model to exactly those sentences
python3 /finstreder/finstreder/create_fixedG.py \
  --workdir /finstreder/tests/outputs/txt-fixed/ \
  --sentences_path /finstreder/tests/data/sentences_mini.txt

# Build fsts from text representation
cat tlg/token.txt | fstcompile --isymbols=symbols/tokens.syms --osymbols=symbols/chars.syms > tlg/token.fst
cat tlg/lexicon.txt | fstcompile --isymbols=symbols/chars.syms --osymbols=symbols/words.syms > tlg/lexicon.fst
cat tlg/grammar.txt | fstcompile --isymbols=symbols/words.syms --osymbols=symbols/words.syms > tlg/grammar.fst

# Optionally draw them as image (run this only if the FSTs are small, like in this example)
fstdraw --isymbols=symbols/tokens.syms --osymbols=symbols/chars.syms --width=120.0 --height=20.0 -portrait tlg/token.fst | dot -Tjpg > token.jpg
fstdraw --isymbols=symbols/chars.syms --osymbols=symbols/words.syms --width=120.0 --height=20.0 -portrait tlg/lexicon.fst | dot -Tjpg > lexicon.jpg
fstdraw --isymbols=symbols/words.syms --osymbols=symbols/words.syms --width=120.0 --height=20.0 -portrait tlg/grammar.fst | dot -Tjpg > grammar.jpg
```

The Token-FST maps from CTC-labels to standard characters.
Repeated labels have to be merged to one single label, except they are separated by a _blank_ label.
For example: _"aaab ab-b"_ -> _"ab abb"_.
Each transition has the required input on the left and the output on the right side.
To allow the usage of labels in _sentencepiece_-style, where a label can include multiple characters, those pieces are split into single characters using additional _epsilon_ transitions (you can run the unit-test and draw the corresponding token graph yourself afterwards).

<div align="center">
    <img src="extras/token.jpg" alt="Token-FST" width="60%"/>
</div>

The Lexicon-FST (left) describes how to build words with those characters.
And the Grammar-FST (right) specifies the sentences a user can say.
Note that in the case of this example a user can only say the two sentences "_ab ab_" or _"abba abba_", nothing else.
To allow arbitrary word combinations, a probabilistic Grammar-FST is required. It can be built from an _n-gram_ model.

<div align="center">
    <img src="extras/lexicon.jpg" alt="Lexicon-FST" width="70%"/>
    <img src="extras/grammar.jpg" alt="Grammar-FST" width="25%"/>
</div>

<br>

Compose and optimize FSTs:

```bash
# Optimize grammar for a faster composition
fstrmepsilon tlg/grammar.fst | fstdeterminize | fstminimize > tlg/grammar_opt.fst

# Build and optimize LG-fst
fstcompose tlg/lexicon.fst tlg/grammar_opt.fst > tlg/lg.fst
fstrmepsilon tlg/lg.fst | fstdeterminize | fstminimize > tlg/lg_opt.fst

# Again, optionally they can be drawn as image
fstdraw --isymbols=symbols/tokens.syms --osymbols=symbols/words.syms --width=120.0 --height=20.0 -portrait tlg/lg.fst | dot -Tjpg > lg.jpg
fstdraw --isymbols=symbols/tokens.syms --osymbols=symbols/words.syms --width=120.0 --height=20.0 -portrait tlg/lg_opt.fst | dot -Tjpg > lg_opt.jpg
```

The combined Lexicon-Grammar-FST maps from characters to sentences. The optimization step removes duplicated paths.

<div align="center">
    <img src="extras/lg_opt.jpg" alt="Lexicon-Grammar-FST" width="95%"/>
</div>

<br>

Build and test example inputs (the input also has to be a FST) \
(This step can be skipped by calling the python bindings directly, see later)

```bash
# Build text representation
python3 /finstreder/tests/test_create_inputs.py \
  --workdir /finstreder/tests/outputs/txt-fixed/

# Build fsts from text representation
cat inputs/input1.txt | fstcompile --isymbols=symbols/tokens.syms --acceptor > inputs/input1.fst
cat inputs/input2.txt | fstcompile --isymbols=symbols/tokens.syms --acceptor > inputs/input2.fst
cat inputs/input3.txt | fstcompile --isymbols=symbols/tokens.syms --acceptor > inputs/input3.fst

# And they can be drawn as image too
fstdraw --isymbols=symbols/tokens.syms --osymbols=symbols/tokens.syms --width=120.0 --height=20.0 -portrait inputs/input1.fst | dot -Tjpg > input1.jpg
fstdraw --isymbols=symbols/tokens.syms --osymbols=symbols/tokens.syms --width=120.0 --height=20.0 -portrait inputs/input2.fst | dot -Tjpg > input2.jpg
fstdraw --isymbols=symbols/tokens.syms --osymbols=symbols/tokens.syms --width=120.0 --height=20.0 -portrait inputs/input3.fst | dot -Tjpg > input3.jpg

# Compose the input with the Token-FST and the result with the LG-FST and get the two best matching results (the output is a FST again)
fstcompose inputs/input1.fst tlg/token.fst | fstcompose - tlg/lg_opt.fst | fstshortestpath --nshortest=2 | fstproject --project_type="output" | fstrmepsilon | fsttopsort | fstpush --to_final=true --push_weights=true > inputs/output1.fst
fstcompose inputs/input2.fst tlg/token.fst | fstcompose - tlg/lg_opt.fst | fstshortestpath --nshortest=2 | fstproject --project_type="output" | fstrmepsilon | fsttopsort | fstpush --to_final=true --push_weights=true > inputs/output2.fst
fstcompose inputs/input3.fst tlg/token.fst | fstcompose - tlg/lg_opt.fst | fstshortestpath --nshortest=2 | fstproject --project_type="output" | fstrmepsilon | fsttopsort | fstpush --to_final=true --push_weights=true > inputs/output3.fst

# Draw them to check the decoded text
fstdraw --isymbols=symbols/words.syms --osymbols=symbols/words.syms --width=120.0 --height=20.0 -portrait inputs/output1.fst | dot -Tjpg > output1.jpg
fstdraw --isymbols=symbols/words.syms --osymbols=symbols/words.syms --width=120.0 --height=20.0 -portrait inputs/output2.fst | dot -Tjpg > output2.jpg
fstdraw --isymbols=symbols/words.syms --osymbols=symbols/words.syms --width=120.0 --height=20.0 -portrait inputs/output3.fst | dot -Tjpg > output3.jpg
```

The Input-FST (left) has one state for each time-step with a transition for each character.
The weights are normalized softmax-probabilities (lower is better here).
The most probable (shortest) path of this input is "_a,b,\<space>,a,b_".
It yields an Output-FST (right) where the shortest (most probable) path is the sentence: "_ab ab_".

<div align="center">
    <img src="extras/input1.jpg" alt="Input-FST" width="75%"/>
    <img src="extras/output1.jpg" alt="Grammar-FST" width="20%"/>
</div>

<br>

### 2) Intent Grammar

If the Grammar-FST is adjusted a little bit, it is able to include intent and entity data.
This can be used for direct Speech-to-Intent transcription, thus replacing the need for an extra NLU service.

```bash
# Switch working directory
cd /finstreder/tests/outputs/slu-fixed/

# Create one file with sentences for each intent, with alternatives already expanded
python3 /finstreder/finstreder/build_ngram_sentences.py \
  --workdir /finstreder/tests/outputs/slu-fixed/ \
  --nlufile_path /finstreder/tests/data/riddles.json

# Create symbols like before
python3 /finstreder/finstreder/create_symbols.py \
  --sentences_path /finstreder/tests/data/riddles.json \
  --alphabet_path /finstreder/tests/data/alphabet_en_chars.json \
  --workdir /finstreder/tests/outputs/slu-fixed/
```

<br>

Now there are two options, the first is the same like above and builds a fixed grammar where the input has to match the example sentences exactly.
While this can be interesting for very small models, a more general approach is the usage of n-grams instead of full sentences to build the grammar model, which is the second option.

```bash
# Build fixed grammar for each intent. Use this now.
python3 /finstreder/finstreder/create_fixedG.py \
  --workdir /finstreder/tests/outputs/slu-fixed/

# Alternative: Build n-grams for each intent
python3 /finstreder/finstreder/build_ngrams.py \
  --workdir /finstreder/tests/outputs/slu-ngram/ \
  --order 3
```

<br>

Instead of differentiating multiple intents by the filename, this information can be inserted into the graph directly.

```bash
# Forward all final states of the sentences/ngrams to a epsilon transition which adds intent names
python3 /finstreder/finstreder/extend_ngrams.py \
  --workdir /finstreder/tests/outputs/slu-fixed/

# Build the riddle solutions intent ...
cat intents/skill_riddles-check_riddle.txt | fstcompile --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms | fstrmepsilon | fstdeterminize | fstminimize > skill_riddles-check_riddle.fst
# ... and draw it
fstdraw --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms --width=120.0 --height=20.0 -portrait skill_riddles-check_riddle.fst | dot -Tjpg > skill_riddles-check_riddle.jpg
```

The resulting FST will now accept one of sample utterances and will add the corresponding intent's name to the output words. The entity placeholders which will be replaced in the next steps.

<div align="center">
    <img src="extras/skill_riddles-check_riddle.jpg" alt="Fixed-Intent-FST" width="95%"/>
</div>

<br>

```bash
# Build fsts for the slot possibilities
python3 /finstreder/finstreder/build_slot_fsts.py \
  --nlufile_path /finstreder/tests/data/riddles.json \
  --workdir /finstreder/tests/outputs/slu-fixed/

# Build the slot automaton ...
cat slots/skill_riddles-riddle_answers.txt | fstcompile --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms > skill_riddles-riddle_answers.fst
# ... and draw it
fstdraw --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms --width=120.0 --height=20.0 -portrait skill_riddles-riddle_answers.fst | dot -Tjpg > skill_riddles-riddle_answers.jpg
```

The Slot-FST accepts all possible entity values and adds brackets at the start and end, that the values can be extracted later. It also handles synonym replacements (see "->" symbols).

<div align="center">
    <img src="extras/skill_riddles-riddle_answers.jpg" alt="Slot-FST" width="95%"/>
</div>

<br>

Now join the FSTs for intents and slots together:

```bash
# Which intent has which slots?
python3 /finstreder/finstreder/map_intents_slots.py \
  --nlufile_path /finstreder/tests/data/riddles.json \
  --workdir /finstreder/tests/outputs/slu-fixed/

# Replace entity placeholders in the intent-fsts
python3 /finstreder/finstreder/merge_intent_fsts.py \
  --workdir /finstreder/tests/outputs/slu-fixed/

# Draw the combined automaton
fstdraw --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms --width=120.0 --height=20.0 -portrait merged/skill_riddles-check_riddle.fst | dot -Tjpg > merged-check_riddle.jpg
```

As shown in below image, the Slot-FSTs are inserted directly before the entity placeholders.

<div align="center">
    <img src="extras/merged-check_riddle.jpg" alt="Merged-FST" width="95%"/>
</div>

<br>

The last step is to create the FSTs for Token and Lexicon and composing the Lexicon with the combined Intent-Slot automaton.

```bash
# Create and build token and lexicon fsts using steps from above

# Optimize L and compose with Intent-FSTs
python3 /finstreder/finstreder/compose_LG_slufsts.py \
  --workdir /finstreder/tests/outputs/slu-fixed/
```

<br>

Let's test it with an input:

```bash
# Build example input
python3 /finstreder/tests/test_create_inputs.py \
  --workdir /finstreder/tests/outputs/slu-fixed/
cat inputs/input4.txt | fstcompile --isymbols=symbols/tokens.syms --acceptor > inputs/input4.fst
fstdraw --isymbols=symbols/tokens.syms --osymbols=symbols/tokens.syms --width=120.0 --height=20.0 -portrait inputs/input4.fst | dot -Tjpg > input4.jpg

# Decode the test input
fstcompose inputs/input4.fst results/token.fst | fstcompose - results/intents/skill_riddles-check_riddle.fst | fstshortestpath --nshortest=1 | fstproject --project_type="output" | fstrmepsilon | fsttopsort | fstpush --to_final=true --push_weights=true > inputs/output4.fst
fstdraw --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms --width=120.0 --height=20.0 -portrait inputs/output4.fst | dot -Tjpg > output4.jpg
```

Going through the Output-FST and collecting the outputs of each transitions, gives the expected sentence which can be easily converted into a response dictionary.

<div align="center">
    <img src="extras/output4.jpg" alt="Intent-Output-FST" width="95%"/>
</div>

<br>

### 3) Usage in code:

Use above steps to build the FSTs. \
Alternatively all the commands (except image drawing) can be run to together with the following script:

```bash
# Optionally as n-gram model or using a fixed grammar like in the example
# Run from parent directory of the workdir or above
python3 /finstreder/finstreder/fullbuild_slu.py \
  --workdir /finstreder/tests/outputs/slu-fixed/ \
  --nlufile_path /finstreder/tests/data/riddles.json \
  --alphabet_path /finstreder/tests/data/alphabet_en_chars.json \
  [--order 3] [--fixed_grammar]
```

Decoding in python:

```python
import json
from finstreder import decoding_tools

# Alphabet as list of characters
with open("/finstreder/tests/data/alphabet_en_chars.json", "r", encoding="utf-8") as file:
    alphabet = json.load(file)

# Input of softmax-probabilities in format: List[List[float]] with shape [timesteps, characters]
from runpy import run_path
ctip = run_path("/finstreder/tests/test_create_inputs.py")
ctc_tokens = ctip["ctc_tokens4"]

workdir = "/finstreder/tests/outputs/slu-fixed/"
decoder = decoding_tools.Decoder(
    path_LGs=workdir+"results/intents/",
    path_T=workdir+"results/token.fst",
    path_token_syms=workdir+"symbols/tokens.syms",
    path_word_syms=workdir+"symbols/nlu.syms",
    alphabet=alphabet,
    fix_alphabet=True,
    weight_scale=22,
    prob_scale=0.5,
    filter_topk=7,
    mean_topk=21,
)

output = decoder.decode2intent(ctc_tokens)
print(output)
# {
#   'intent': {'name': 'skill_riddles-check_riddle'},
#   'entities': [{'entity': 'skill_riddles-riddle_answers', 'value': 'a pet'}],
#   'text': 'the solution is dog'
# }
```

<br>

## Citation

Please cite finstreder if you found it helpful for your research or business.

```bibtex
@article{
  finstreder,
  title={{Finstreder: Simple and fast Spoken Language Understanding with Finite State Transducers using modern Speech-to-Text models}},
  author={Bermuth, Daniel and Poeppel, Alexander and Reif, Wolfgang},
  journal={arXiv preprint arXiv:2206.14589},
  year={2022}
}
```

<br>

## Testing

Build container with testing tools:

```bash
docker build -f tests/Containerfile -t testing_finstreder .

docker run --network host --rm \
  --volume `pwd`/:/finstreder/ \
  -it testing_finstreder
```

Run unit tests:

```bash
pip3 install --user -e /finstreder/
pytest /finstreder/ --cov=finstreder -v -s
```

For syntax tests, check out the steps in the [gitlab-ci](.gitlab-ci.yml#L53) file.

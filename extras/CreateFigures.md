# Create paper Figures

```bash
cd /finstreder/tests/outputs/

# Create all FSTs
python3 /finstreder/finstreder/fullbuild_slu.py \
  --workdir /finstreder/tests/outputs/slu-paper/ \
  --nlufile_path /finstreder/tests/data/cuteanimals.json \
  --alphabet_path /finstreder/tests/data/alphabet_en_chars.json \
  --order 0 --fixed_grammar

cd /finstreder/tests/outputs/slu-paper/

# Replace <epsilon> with <eps>
find . -type f -exec sed -i 's/<epsilon>/<eps>/g' {} +

# Draw intent
cat intents/get-looks.txt | fstcompile --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms | fstrmepsilon | fstdeterminize | fstminimize | \
fstdraw --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms --width=120.0 --height=20.0 -portrait | dot -Tjpg > itc_intent.jpg

# Draw slot
cat slots/animal.txt | fstcompile --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms | fstrmepsilon | fstdeterminize | fstminimize | \
fstdraw --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms --width=120.0 --height=20.0 -portrait --fontsize=24 | dot -Tjpg > itc_animal.jpg

# Draw merged
fstrmepsilon merged/get-looks.fst | fstdeterminize | fstminimize | \
fstdraw --isymbols=symbols/nlu.syms --osymbols=symbols/nlu.syms --width=120.0 --height=20.0 -portrait | dot -Tjpg > itc_merged.jpg
```

Use some image editing tool to optimize the figures.

import argparse
import json
import os
import re
import shutil
import subprocess
from distutils import dir_util

import tqdm

# ==================================================================================================

slots_dirpath = "slots/"
intents_dirpath = "intents/"
symbols_filepath = "symbols/nlu.syms"
slot_map_filepath = "slots/intent_slot_map.json"
output_dirpath = "merged/"
tlg_dirpath = "tlg/"
tmp_dir = "/tmp/finstreder/"

# ==================================================================================================


def build_fsts(input_dir: str, symbols_path: str, ftype: str) -> None:
    fst_files = os.listdir(input_dir)
    fst_files = [f for f in fst_files if f.endswith(".txt")]
    fst_files = [os.path.join(input_dir, f) for f in fst_files]
    fst_files = [f.replace(".txt", "") for f in fst_files]

    print("Building {} fsts from text representation ...".format(ftype))
    for fstfile in tqdm.tqdm(fst_files):
        cmd = "cat {}.txt | fstcompile --isymbols={} --osymbols={} > {}.fst"
        cmd = cmd.format(fstfile, symbols_path, symbols_path, fstfile)
        subprocess.call(["/bin/bash", "-c", cmd])

    if ftype == "slot":
        # Optimizing the intent FSTs did result in a slower inference, they also can't be optimized
        # further after slot injection, it will fail with: "Unequal arguments (non-functional FST?)"

        print("Optimizing {} fsts ...".format(ftype))
        for fstfile in tqdm.tqdm(fst_files):
            tmp_path = "{}{}.fst".format(tmp_dir, os.path.basename(fstfile))

            cmd = "fstrmepsilon {}.fst | fstdeterminize | fstminimize > {}"
            cmd = cmd.format(fstfile, tmp_path)
            subprocess.call(["/bin/bash", "-c", cmd])
            shutil.copyfile(tmp_path, "{}.fst".format(fstfile))


# ==================================================================================================


def inject_slots(
    intents_dir: str,
    slots_dir: str,
    symbols_path: str,
    slot_map_path: str,
    output_dir: str,
) -> None:
    print("Injecting slot fsts into intent fsts ...")

    # Load all symbols that we can later get the id of the slot names
    with open(symbols_path, "r", encoding="utf-8") as file:
        symbols = file.readlines()
    syms = [s.strip().split("\t") for s in symbols]
    symbol_map = {s[0]: s[1] for s in syms}

    with open(slot_map_path, "r", encoding="utf-8") as file:
        slot_map = json.load(file)

    # Copy all fsts, that they can later be updated in-place
    dir_util.copy_tree(intents_dir, output_dir)

    for intent in tqdm.tqdm(slot_map):
        intent_path = os.path.join(output_dir, "{}.fst".format(intent))

        for slot in slot_map[intent]:
            # Use the same slot-fst for named and unnamed slots, but keep name in return arc
            if "?" in slot:
                slot_name = re.sub(r"\?[a-zA-Z0-9_]+", "", slot)
            else:
                slot_name = slot

            slot_label = symbol_map["({})".format(slot)]
            slot_path = os.path.join(slots_dir, "{}.fst".format(slot_name))
            tmp_path = "{}{}.fst".format(tmp_dir, intent)

            cmd = "fstreplace --call_arc_labeling=neither --return_arc_labeling=output"
            cmd += " --return_label={} {} -1 {} {} > {}"
            cmd = cmd.format(slot_label, intent_path, slot_path, slot_label, tmp_path)

            subprocess.call(["/bin/bash", "-c", cmd])
            shutil.copyfile(tmp_path, intent_path)


# ==================================================================================================


def merge_fsts(workdir: str) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if not os.path.isdir(tmp_dir):
        os.makedirs(tmp_dir)

    intents_dir = os.path.join(workdir, intents_dirpath)
    slots_dir = os.path.join(workdir, slots_dirpath)
    symbols_path = os.path.join(workdir, symbols_filepath)
    slot_map_path = os.path.join(workdir, slot_map_filepath)

    build_fsts(intents_dir, symbols_path, ftype="intent")
    build_fsts(slots_dir, symbols_path, ftype="slot")
    inject_slots(
        intents_dir,
        slots_dir,
        symbols_path,
        slot_map_path,
        output_dir,
    )


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    merge_fsts(args.workdir)


# ==================================================================================================

if __name__ == "__main__":
    main()

import math
import multiprocessing as mp
import os
import random
import re
from concurrent.futures import ProcessPoolExecutor as Pool
from functools import partial
from typing import Any, Dict, List, Tuple

import numpy as np

# An API for pywrapfst can be found here: http://mi.eng.cam.ac.uk/~wjb31/data/pywrapfst.1.6.8.html
# It's not up to date, but should include the most important commands
import pywrapfst as fst

# ==================================================================================================


def softmax(x: List[List[float]], axis: int = 1) -> List[List[float]]:
    sx = np.array(x)
    sx = np.exp(sx) / np.sum(np.exp(sx), axis=axis, keepdims=True)
    x = sx.tolist()
    return x


# ==================================================================================================


def rescale(x: List[List[float]], axis: int = 1) -> List[List[float]]:
    sx = np.array(x)
    sx = sx / np.sum(sx, axis=axis, keepdims=True)
    x = sx.tolist()
    return x


# ==================================================================================================


def build_input_fst(
    ctc_input: List[List[float]],
    alphabet: list,
    weight_scale: float = 1.0,
    prob_scale: float = 1.0,
    filter_topk: int = 0,
    mean_topk: int = 0,
) -> str:
    """Build an Input-FST in string format. See Decoder class for parameter description."""

    # Add additional space at the end for decoding
    extra_space = [0.001 for _ in ctc_input[0]]
    extra_space[alphabet.index("<space>")] = 0.99
    ctc_input.extend(softmax([extra_space]))

    if prob_scale != 1:
        # Scale probabilities with exponential factor and afterwards linear that they sum to 1 again
        ctc_input = np.power(np.array(ctc_input), prob_scale).tolist()
        ctc_input = rescale(ctc_input)

    if mean_topk > 0:
        # Calculate mean of probabilities of the mean-topk highest value in each timestep
        meanprob = 0.0
        for step in ctc_input:
            mv = sorted(step)[-mean_topk]
            meanprob += mv
        meanprob = meanprob / len(ctc_input)

    fst_str = ""
    for i, step in enumerate(ctc_input):
        # Normalizing denominator of softmax
        # See: https://github.com/mjansche/ctc_sampling/blob/master/data/logits2fsts.py#L30
        z = math.log(math.fsum(np.exp(step, dtype=np.float64)))

        cidprob = list(enumerate(step))
        if filter_topk > 0:
            # Keep only the best filter-topk options per char
            cidprob = sorted(cidprob, key=lambda k: k[1])[-filter_topk:]

        for idx, prob in cidprob:
            if mean_topk > 0 and prob < meanprob:
                # Skip this because it's probability is to low
                continue

            weight = z - prob

            # Scale weights to allow adjusting total weight of ctc-outputs vs n-gram probabilities
            weight = weight * weight_scale

            fst_str += "{} {} {} {:.5f}\n".format(i, i + 1, alphabet[idx], weight)

    fst_str += "{}\n".format(len(ctc_input))
    return fst_str


# ==================================================================================================


def text2ctc(text: str, alphabet: list) -> List[List[float]]:
    """Convert text to ctc labels"""

    ctc_labels = []
    for char in text:
        if char == " ":
            char = "<space>"

        # For stability reasons, use a small probability for the other characters
        step = [random.uniform(0.01, 0.001) for _ in range(len(alphabet))]
        step[alphabet.index(char)] = 0.99
        ctc_labels.append(step)

        # Adding an extra blank label greatly improves accuracy
        step = [random.uniform(0.001, 0.002) for _ in range(len(alphabet))]
        step[alphabet.index("<blank>")] = 0.99
        ctc_labels.append(step)

    ctc_labels = rescale(ctc_labels)
    return ctc_labels


# ==================================================================================================


def intentsample2components(sample: str) -> List[str]:
    splitter = r" (?![^(]*\))(?![^[]*\])"
    components = re.split(splitter, sample)
    return components


# ==================================================================================================


def extract_text2intent(text: str) -> dict:
    decoded: Dict[str, Any] = {
        "intent": {
            "name": "",
        },
        "entities": [],
        "text": "",
    }

    if "((" not in text:
        return decoded

    sentence, intent = text.split("((")
    intent = intent.strip()[:-2]
    decoded["intent"]["name"] = intent

    sentence = sentence.replace("] (", "](")
    components = intentsample2components(sentence)
    spoken_text = ""

    for component in components:
        if component.startswith("["):
            # Slot
            value, entity = component[1:-1].split("](")
            slot = {}

            if "?" in entity:
                # Slot with role
                entity, role = entity.split("?")
                slot["entity"] = entity.strip()
                slot["role"] = role.strip()
            else:
                slot["entity"] = entity.strip()

            if "->" in value:
                # Value with synonym
                value, synonym = value.split("->")
                slot["value"] = synonym.strip()
            else:
                slot["value"] = value.strip()

            spoken_text = spoken_text + value.strip() + " "
            decoded["entities"].append(slot)

        else:
            # Normal word
            spoken_text = spoken_text + component + " "

    decoded["text"] = spoken_text.strip()
    return decoded


# ==================================================================================================


class Decoder:
    def __init__(
        self,
        path_LGs: str,
        path_T: str,
        path_token_syms: str,
        path_word_syms: str,
        alphabet: List[str],
        fix_alphabet: bool = True,
        weight_scale: float = 1.0,
        prob_scale: float = 1.0,
        filter_topk: int = 0,
        mean_topk: int = 0,
    ) -> None:
        """
        Decoding class for CTC-inputs with FSTs

        Parameters
        ----------
        alphabet: should contain a <blank> symbol and using <space> instead of " "
        fix_alphabet: appends a <blank> symbol to the alphabet and replaces
                      any of [" ", "▁", <unk>] with <space>
        weight_scale: adjust relative weighting between input and grammar model if a
                      probabilistic ngram model is used for decoding
        prob_scale: allows exponential scaling of ctc-probabilities to adjust relative weighting
                    between characters
        filter_topk: use only the ctc-tokens with topk-highest probabilities to speed up prediction
        mean_topk: drop tokens with probabilities lower than the average of the mean-topk highest
                   probability value over all the timesteps
        """

        if fix_alphabet:
            alphabet = [a.replace("<unk>", " ") for a in alphabet]
            alphabet = [a.replace("▁", " ") for a in alphabet]
            alphabet = [a.replace(" ", "<space>") for a in alphabet]
            alphabet.append("<blank>")

        self.alphabet = alphabet
        self.idx2char = dict(enumerate(alphabet))

        self.weight_scale = weight_scale
        self.prob_scale = prob_scale
        self.filter_topk = filter_topk
        self.mean_topk = mean_topk

        lgpaths = os.listdir(path_LGs)
        lgpaths = [os.path.join(path_LGs, lp) for lp in lgpaths]
        self.LGs = [fst.Fst.read(lp) for lp in lgpaths]
        self.T = fst.Fst.read(path_T)

        self.token_syms = fst.SymbolTable.read_text(path_token_syms)
        self.word_syms = fst.SymbolTable.read_text(path_word_syms)

    # ==============================================================================================

    def decode_greedy_text(self, ctc_input: List[List[float]]) -> str:
        """Greedy ctc to text decoding"""

        # Merge repeated characters
        values = np.argmax(ctc_input, axis=-1)
        merged = [values[0]]
        for v in values[1:]:
            if v != merged[-1]:
                merged.append(v)

        blank_id = len(self.alphabet) - 1
        merged = [v for v in merged if not v == blank_id]
        gd_text = "".join([self.idx2char[v] for v in merged])

        # Replace spaces
        gd_text = gd_text.replace("<space>", " ")
        gd_text = gd_text.strip()
        return gd_text

    # ==============================================================================================

    def decode2text(self, ctc_input: List[List[float]]) -> str:
        """Decode the CTC-labels to the most probable text transcription"""

        # Build Input-FST from the CTC-labels
        input_fst = self._compile_input_fst(ctc_input)

        # Compose with TLG-FST to get the Output-FST
        output_fst, _ = self._compose_input_path(self.LGs[0], input_fst)

        # Extract the output symbols from the FST
        text = self._output_fst2text(output_fst)

        return text

    # ==============================================================================================

    def decode2intent(self, ctc_input: List[List[float]]) -> dict:
        """Decode the CTC-labels and extract intents and entities"""

        text = self._decode_text_parallel(ctc_input)
        decoded = extract_text2intent(text)
        decoded["greedy"] = self.decode_greedy_text(ctc_input)
        return decoded

    # ==============================================================================================

    def text2intent(self, text: str) -> dict:
        """Decode textual input to intent and entities"""

        ctc_labels = text2ctc(text, self.alphabet)
        decoded = self.decode2intent(ctc_labels)
        return decoded

    # ==============================================================================================

    def _compile_input_fst(self, ctc_input: List[List[float]]) -> fst.Fst:
        """Build Input-FST from the CTC-labels"""

        # Build string representation
        input_fst_str = build_input_fst(
            ctc_input,
            self.alphabet,
            self.weight_scale,
            self.prob_scale,
            self.filter_topk,
            self.mean_topk,
        )

        # Compile to FST-object
        compiler = fst.Compiler(
            isymbols=self.token_syms, acceptor=True, keep_isymbols=True
        )
        compiler.write(input_fst_str)
        input_fst = compiler.compile()
        input_fst.arcsort()

        # Compose with Token-FST to speed up later parallel composition with LG-FST
        # (didn't show an effect to performance in simple compositions)
        input_fst = fst.compose(input_fst, self.T)

        return input_fst

    # ==============================================================================================

    def _decode_text_parallel(self, ctc_input: List[List[float]]) -> str:
        """Decode the CTC-labels using multiple FSTs and return the best match"""

        # Build Input-FST from the CTC-labels
        input_fst = self._compile_input_fst(ctc_input)

        # Get pathweight for each intent
        if len(self.LGs) < 4:
            # Non parallel call runs faster for small assistants
            outputs = [self._compose_input_path(tgf, input_fst) for tgf in self.LGs]
        else:
            pfunc = partial(self._compose_input_path, input_fst=input_fst)
            workers = min(mp.cpu_count(), len(self.LGs))
            with Pool(workers) as p:
                outputs = list(p.map(pfunc, self.LGs))

        # Find the best matching intent
        best_fst = sorted(outputs, key=lambda k: k[1])[0][0]

        # Extract the output symbols from the FST
        text = self._output_fst2text(best_fst)

        return text

    # ==============================================================================================

    @staticmethod
    def _compose_input_path(
        target_fst: fst.Fst, input_fst: fst.Fst
    ) -> Tuple[fst.Fst, float]:
        """Compose Input-FST with LG-FST to get the Output-FST"""

        # Compose
        output_fst = fst.compose(input_fst, target_fst)
        output_fst.project("output")
        output_fst = fst.shortestpath(output_fst, nshortest=1)
        output_fst.rmepsilon()
        output_fst.topsort()
        output_fst = fst.push(output_fst, to_final=True, push_weights=True)

        # Get weight of path which is saved in the last state
        state = None
        for state in output_fst.states():
            pass
        if state is not None:
            pathweight = float(output_fst.final(state))
        else:
            pathweight = float("inf")

        return output_fst, pathweight

    # ==============================================================================================

    def _output_fst2text(self, output_fst: fst.Fst) -> str:
        """Extract the output symbols from the FST"""

        text = ""
        for state in output_fst.states():
            # Since the FST has only one path, we can just loop from one state to the next

            for arc in output_fst.arcs(state):
                text = text + self.word_syms.find(arc.olabel) + " "
        text = text.strip()

        return text

import argparse
import os

import tqdm

# ==================================================================================================

input_dirpath = "ngrams/"
output_dirpath = "intents/"

# ==================================================================================================


def extend_with_intents(input_dir: str, output_dir: str) -> None:
    print("Adding intents to ngrams ...")

    ngram_files = os.listdir(input_dir)
    ngram_files = [f for f in ngram_files if f.endswith(".txt")]

    for fpath in tqdm.tqdm(ngram_files):
        input_file = os.path.join(input_dir, fpath)

        with open(input_file, "r", encoding="utf-8") as file:
            fst_str = file.readlines()
        fst_str = [s.strip() for s in fst_str]

        # Find id of last state
        max_idx = 0
        for line in fst_str:
            parts = line.split("\t")
            if len(parts) > 2:
                max_idx = max(max_idx, int(parts[0]), int(parts[1]))

        # Connect all old final states with a new state
        fst_str_new = []
        for line in fst_str:
            parts = line.split("\t")
            if len(parts) > 2:
                fst_str_new.append(line)
            else:
                fsn = "{}\t{}\t<epsilon>\t<epsilon>\t{}"
                fsn = fsn.format(parts[0], max_idx + 1, parts[1])
                fst_str_new.append(fsn)

        # Add intent transitions as new final states
        fsn = "{}\t{}\t<epsilon>\t{}"
        intent = "(({}))".format(fpath.replace(".txt", ""))
        fst_str_new.append(fsn.format(max_idx + 1, max_idx + 2, intent))
        fst_str_new.append("{}".format(max_idx + 2))

        output_file = os.path.join(output_dir, fpath)
        with open(output_file, "w+", encoding="utf-8") as file:
            file.write("\n".join(fst_str_new))


# ==================================================================================================


def extend_ngrams(workdir: str) -> None:
    input_dir = os.path.join(workdir, input_dirpath)
    output_dir = os.path.join(workdir, output_dirpath)

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    extend_with_intents(input_dir, output_dir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    extend_ngrams(args.workdir)


# ==================================================================================================

if __name__ == "__main__":
    main()

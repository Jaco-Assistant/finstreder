import argparse
import os
import subprocess

import tqdm

# ==================================================================================================

input_dirpath = "merged/"
output_dirpath = "results/intents/"
tlg_dirpath = "tlg/"
relabel_filepath = "symbols/relabel.pairs"

# ==================================================================================================


def build_TL_fst(workdir: str) -> None:
    print("Creating and optimizing Token and Lexcion FSTs ...")
    cdcmd = "cd {} && ".format(workdir)

    if not os.path.isdir(workdir + "results/"):
        os.makedirs(workdir + "results/")
    cmd = "cat tlg/token.txt | fstcompile --isymbols=symbols/tokens.syms --osymbols=symbols/chars.syms > results/token.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])

    cmd = "cat tlg/lexicon.txt | fstcompile --isymbols=symbols/chars.syms --osymbols=symbols/words.syms > tlg/lexicon.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])

    cmd = "fstrmepsilon tlg/lexicon.fst | fstdeterminize | fstminimize > tlg/lexicon_opt.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])

    cmd = "fstconvert --fst_type=olabel_lookahead --save_relabel_opairs=symbols/relabel.pairs tlg/lexicon_opt.fst > tlg/lexicon_lookahead.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])


# ==================================================================================================


def compose_LG_fsts(workdir: str) -> None:
    print("Composing Lexicon with Intent-FSTs ...")

    input_dir = os.path.join(workdir, input_dirpath)
    output_dir = os.path.join(workdir, output_dirpath)
    tlg_dir = os.path.join(workdir, tlg_dirpath)
    relabel_path = os.path.join(workdir, relabel_filepath)

    files = os.listdir(input_dir)
    files = [f for f in files if f.endswith(".fst")]

    for fpath in tqdm.tqdm(files):
        inpath = os.path.join(input_dir, fpath)
        rlpath = inpath.replace(".fst", "_relabeled.fst")
        outpath = os.path.join(output_dir, fpath)

        cmd = "fstrelabel --relabel_ipairs={} {} | fstarcsort --sort_type=ilabel > {}"
        cmd = cmd.format(relabel_path, inpath, rlpath)
        subprocess.call(["/bin/bash", "-c", cmd])

        cmd = "fstcompose {}lexicon_lookahead.fst {} > {}"
        subprocess.call(["/bin/bash", "-c", cmd.format(tlg_dir, rlpath, outpath)])


# ==================================================================================================


def create_fsts(workdir: str) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    build_TL_fst(workdir)
    compose_LG_fsts(workdir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    create_fsts(args.workdir)


# ==================================================================================================

if __name__ == "__main__":
    main()

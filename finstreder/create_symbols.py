import argparse
import json
import os
import re
from typing import List, Set, Tuple

import tqdm

# ==================================================================================================

output_dirpath = "symbols/"

# ==================================================================================================


def load_alphabet(alphabet_path: str) -> Tuple[List[str], List[str]]:
    with open(alphabet_path, "r", encoding="utf-8") as file:
        alphabet: List[str] = json.load(file)

    # Replace special symbols
    alphabet = [a.replace("<unk>", " ") for a in alphabet]
    alphabet = [a.replace("▁", " ") for a in alphabet]
    alphabet = sorted(set(alphabet))

    # Collect unique chars in the alphabet
    acs_list = [set(a) for a in alphabet]
    alphabet_chars = sorted(set().union(*acs_list))

    return alphabet, alphabet_chars


# ==================================================================================================


def collect_words_txt(sentences_path: str) -> Set[str]:
    """Handles a text file with one sentence per line"""

    with open(sentences_path, "r", encoding="utf-8") as file:
        lines = file.readlines()

    unique_words: Set[str] = set()
    for line in tqdm.tqdm(lines):
        line_lower = line.lower()
        words = [w.strip() for w in line_lower.split()]
        unique_words = unique_words.union(set(words))

    return unique_words


# ==================================================================================================


def intentsample2components(sample: str) -> List[str]:
    splitter = r" (?![^(]*\))(?![^[]*\])"
    components = re.split(splitter, sample)
    return components


# ==================================================================================================


def collect_words_json(sentences_path: str) -> Tuple[Set[str], List[str]]:
    """Handles a nlu file in Jaco's format"""

    print("Collecting words from intents and lookups ...")

    with open(sentences_path, "r", encoding="utf-8") as file:
        nludata = json.load(file)

    unique_words: Set[str] = set()
    lookup_keys: Set[str] = set()

    total_samples = sum((len(nludata["intents"][e]) for e in nludata["intents"]))
    with tqdm.tqdm(total=total_samples) as pbar:
        # Words in intents
        for intent in nludata["intents"]:
            for line in nludata["intents"][intent]:
                components = intentsample2components(line)

                for component in components:
                    if component.startswith("["):
                        # Ignore entity values, they will be added later,
                        # but add entity key to catch named entities
                        entity_key = component.strip().split("(")[1][:-1]
                        lookup_keys = lookup_keys.union({entity_key})

                    elif component.startswith("("):
                        # Split alternatives
                        component = re.sub(r"[\(\|\)]", " ", component)
                        words = [w.strip() for w in component.split(" ")]
                        unique_words = unique_words.union(set(words))

                    else:
                        # Simple word
                        unique_words = unique_words.union({component})

                pbar.update(1)

    total_samples = sum((len(nludata["lookups"][e]) for e in nludata["lookups"]))
    with tqdm.tqdm(total=total_samples) as pbar:
        # Words in lookups
        for lookup in nludata["lookups"]:
            for line in nludata["lookups"][lookup]:
                if "->" in line:
                    line = re.sub(r"[\(\|\)]", " ", line)
                    line = line.replace("->", " ")

                words = [w.strip() for w in line.split(" ")]
                unique_words = unique_words.union(set(words))
                pbar.update(1)

    nlu_keys = list(nludata["intents"].keys())
    nlu_keys = ["({})".format(k) for k in nlu_keys]
    nlu_keys.extend(list(lookup_keys))
    return unique_words, nlu_keys


# ==================================================================================================


def collect_words(sentences_path: str, alphabet_path: str, output_dir: str) -> None:
    # Collect unique words
    if sentences_path.endswith(".txt"):
        unique_words = collect_words_txt(sentences_path)
    elif sentences_path.endswith(".json"):
        unique_words, nlu_keys = collect_words_json(sentences_path)
    else:
        raise ValueError(
            "Sentence format has to be either .txt or .json, got something else"
        )
    unique_words.discard("")
    print("\nFound {} unique words".format(len(unique_words)))
    unique_words_sorted = sorted(unique_words)

    # Filter words with characters not in the alphabet
    _, alphabet_chars = load_alphabet(alphabet_path)
    unique_words_filtered = []
    bad_words = []
    for word in unique_words_sorted:
        if not set(word).issubset(alphabet_chars):
            bad_words.append(word)
        elif word in [" ", "'"]:
            bad_words.append(word)
        else:
            unique_words_filtered.append(word)

    if len(bad_words) > 0:
        print("WARNING: Excluded following words: ", bad_words)

    vocab_str = "<epsilon>\t0\n"
    for i, word in enumerate(unique_words_filtered, start=1):
        vocab_str += "{}\t{}\n".format(word, i)

    output_file = os.path.join(output_dir, "words.syms")
    with open(output_file, "w+", encoding="utf-8") as file:
        file.write(vocab_str)

    if sentences_path.endswith(".json"):
        # Create nlu.syms file with extra words for the nlu syntax symbols
        extra_symbols = ["[", "]", "->"]
        extra_symbols.extend(nlu_keys)

        for i, word in enumerate(extra_symbols, start=len(unique_words) + 1):
            if word in nlu_keys:
                vocab_str += "({})\t{}\n".format(word, i)
            else:
                vocab_str += "{}\t{}\n".format(word, i)

        output_file = os.path.join(output_dir, "nlu.syms")
        with open(output_file, "w+", encoding="utf-8") as file:
            file.write(vocab_str)


# ==================================================================================================


def build_char_maps(alphabet_path: str, output_dir: str) -> None:
    alphabet, alphabet_chars = load_alphabet(alphabet_path)
    alphabet = [a.replace(" ", "<space>") for a in alphabet]
    alphabet_chars = [a.replace(" ", "<space>") for a in alphabet_chars]
    print("Alphabet has {} characters".format(len(alphabet)))

    # Build tokens map
    char_str = "<epsilon>\t0\n"
    for i, char in enumerate(alphabet, start=1):
        char_str += "{}\t{}\n".format(char, i)

    char_str += "{}\t{}\n".format("<blank>", len(alphabet) + 1)
    output_file = os.path.join(output_dir, "tokens.syms")
    with open(output_file, "w+", encoding="utf-8") as file:
        file.write(char_str)

    # Build chars map
    char_str = "<epsilon>\t0\n"
    for i, char in enumerate(alphabet_chars, start=1):
        char_str += "{}\t{}\n".format(char, i)

    output_file = os.path.join(output_dir, "chars.syms")
    with open(output_file, "w+", encoding="utf-8") as file:
        file.write(char_str)


# ==================================================================================================


def create_symbols(workdir: str, sentences_path: str, alphabet_path: str) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    collect_words(sentences_path, alphabet_path, output_dir)
    build_char_maps(alphabet_path, output_dir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--sentences_path",
        help="Path to a text file with sample sentences or a json file with sample intents",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--alphabet_path",
        help="Path to the alphabet.json file",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    create_symbols(args.workdir, args.sentences_path, args.alphabet_path)


# ==================================================================================================

if __name__ == "__main__":
    main()

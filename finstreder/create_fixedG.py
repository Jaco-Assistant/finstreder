import argparse
import os
from typing import Tuple

import tqdm

# ==================================================================================================

output_dirpath_text = "tlg/"
input_dirpath_intents = "sents/"
output_dirpath_intents = "ngrams/"


# ==================================================================================================


def add_simple_words(
    wordstr: str,
    idx: int,
    start_idx: int = -1,
    end_idx: int = -1,
    eps_input: bool = False,
) -> Tuple[str, int]:
    """Add one or multiple words to the graph"""

    fst_str = ""
    words = wordstr.split(" ")
    words = [w.strip() for w in words]

    for i, word in enumerate(words):
        if eps_input:
            inp_word = "<epsilon>"
        else:
            inp_word = word

        if i == 0 and start_idx != -1:
            # First word and fixed start id given

            if i == len(words) - 1 and end_idx != -1:
                # Last word as well and fixed end id given
                fst_str += "{} {} {} {}\n".format(start_idx, end_idx, inp_word, word)
            else:
                fst_str += "{} {} {} {}\n".format(start_idx, idx + 1, inp_word, word)
                idx = idx + 1

        elif i == len(words) - 1 and end_idx != -1:
            # Last word and fixed end id given
            fst_str += "{} {} {} {}\n".format(idx, end_idx, inp_word, word)

        else:
            # Standard word in chain
            fst_str += "{} {} {} {}\n".format(idx, idx + 1, inp_word, word)
            idx = idx + 1

    return fst_str, idx


# ==================================================================================================


def build_simple_text_grammar(sentences_path: str) -> str:
    with open(sentences_path, "r", encoding="utf-8") as file:
        lines = file.readlines()

    idx = 0
    fst_str = ""
    for line in tqdm.tqdm(lines):
        fstr, idx = add_simple_words(line, idx, start_idx=0)
        fst_str += fstr

        # Make each sentence a final state
        fst_str += "{}\n".format(idx)

    return fst_str


# ==================================================================================================


def build_simple_intent_grammar(input_dir: str, output_dir: str) -> None:
    files = os.listdir(input_dir)
    for fpath in files:
        inpath = os.path.join(input_dir, fpath)
        outpath = os.path.join(output_dir, fpath)

        fst_str = build_simple_text_grammar(inpath)
        fst_str = fst_str.replace("\n", " 0\n")
        fst_str = fst_str.replace(" ", "\t")

        with open(outpath, "w+", encoding="utf-8") as file:
            file.write(fst_str)


# ==================================================================================================


def build_grammar(workdir: str, sentences_path: str = "") -> None:
    print("Building fixed grammar fst ...")

    if sentences_path != "":
        output_dir = os.path.join(workdir, output_dirpath_text)
    else:
        output_dir = os.path.join(workdir, output_dirpath_intents)

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if sentences_path != "":
        fst_str = build_simple_text_grammar(sentences_path)

        output_file = os.path.join(output_dir, "grammar.txt")
        with open(output_file, "w+", encoding="utf-8") as file:
            file.write(fst_str)

    else:
        input_dir = os.path.join(workdir, input_dirpath_intents)
        build_simple_intent_grammar(input_dir, output_dir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--sentences_path",
        help="Path to a text file with sample sentences or a json file with sample intents",
        type=str,
        required=False,
        default="",
    )
    args = parser.parse_args()

    build_grammar(args.workdir, args.sentences_path)


# ==================================================================================================

if __name__ == "__main__":
    main()

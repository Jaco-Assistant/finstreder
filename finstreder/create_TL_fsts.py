import argparse
import os

import tqdm

# ==================================================================================================

symbols_dirpath = "symbols/"
output_dirpath = "tlg/"

# ==================================================================================================


def build_token(symbols_dir: str, output_dir: str) -> None:
    print("Building token fst ...")

    input_file = os.path.join(symbols_dir, "tokens.syms")
    with open(input_file, "r", encoding="utf-8") as file:
        lines = file.readlines()

    # Allow start with blank
    fst_str = "0 0 <blank> <epsilon>\n"
    fst_str += "0\n"

    # Create a node map for each distinct token
    idx = 1
    nodemap = {}
    for line in lines:
        token = line.split()[0]
        if token in ("<epsilon>", "<blank>"):
            continue
        nodemap[token] = idx
        idx = idx + 1

    for line in tqdm.tqdm(lines):
        char1 = line.split()[0]
        if char1 in ("<epsilon>", "<blank>"):
            continue

        # If the token is not a single character, split it up
        chars = []
        if char1.startswith("<space>"):
            chars.append("<space>")
        chars.extend(list(char1.replace("<space>", "")))
        if char1.endswith("<space>") and char1 != "<space>":
            chars.append("<space>")

        # Connection for new token
        tokenidx = nodemap[char1]
        fst_str += "0 {} {} <epsilon>\n".format(tokenidx, char1)

        # Connections for token elements
        startidx = tokenidx
        for char in chars:
            idx = idx + 1
            fst_str += "{} {} <epsilon> {}\n".format(startidx, idx, char)
            startidx = idx

        # Connections if next token is the same, or a blank
        fst_str += "{} {} {} <epsilon>\n".format(idx, idx, char1)
        fst_str += "{} 0 {} <epsilon>\n".format(idx, "<blank>")

        # Connections if next char is different
        for line2 in lines:
            char2 = line2.split()[0]
            if char2 in ("<epsilon>", "<blank>", char1):
                continue
            fst_str += "{} {} {} <epsilon>\n".format(idx, nodemap[char2], char2)

        # Make each node a accepting node
        fst_str += "{}\n".format(idx)

    output_file = os.path.join(output_dir, "token.txt")
    with open(output_file, "w+", encoding="utf-8") as file:
        file.write(fst_str)


# ==================================================================================================


def build_lexicon(symbols_dir: str, output_dir: str) -> None:
    print("Building lexicon fst ...")

    input_file = os.path.join(symbols_dir, "words.syms")
    with open(input_file, "r", encoding="utf-8") as file:
        lines = file.readlines()

    idx = 0
    fst_str = ""

    # Accept empty word and allow start with space
    fst_str += "0 1 <epsilon> <epsilon>\n"
    fst_str += "0\n"
    fst_str += "1 1 <space> <epsilon>\n"
    idx = idx + 1

    for line in tqdm.tqdm(lines):
        cols = line.split()
        word = cols[0]
        if word == "<epsilon>":
            continue

        letters = list(word)
        for i, letter in enumerate(letters):
            if i == 0:
                fst_str += "1 {} {} {}\n".format(idx + 1, letter, word)
            else:
                idx = idx + 1
                fst_str += "{} {} {} <epsilon>\n".format(idx, idx + 1, letter)
        idx = idx + 1

        # All words in the lexicon have to end with a <space> which is used as disambiguation symbol here
        # Else determinization of the lexicon-fst might not work if using larger lexicons
        fst_str += "{} {} <space> <epsilon>\n".format(idx, idx + 1)
        idx = idx + 1

        # Allow jump back to start with epsilon
        fst_str += "{} 1 <epsilon> <epsilon>\n".format(idx)
        fst_str += "{}\n".format(idx)

    output_file = os.path.join(output_dir, "lexicon.txt")
    with open(output_file, "w+", encoding="utf-8") as file:
        file.write(fst_str)


# ==================================================================================================


def create_fsts(workdir: str) -> None:
    symbols_dir = os.path.join(workdir, symbols_dirpath)
    output_dir = os.path.join(workdir, output_dirpath)

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    build_token(symbols_dir, output_dir)
    build_lexicon(symbols_dir, output_dir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    create_fsts(args.workdir)


# ==================================================================================================

if __name__ == "__main__":
    main()
